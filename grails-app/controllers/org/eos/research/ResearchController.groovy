package org.eos.research

import research_prototype.DataInitService
import research_prototype.ResearchService

class ResearchController {

    ResearchService researchService
    DataInitService dataInitService

    def chooseProject() {
        def projects = ResearchProject.findAll().sort({a, b -> a.name <=> b.name})

        return [projects: projects]
    }

    def chooseResearcher(ResearchProject project) {
        def characters = Character.findAll().sort({a, b -> a.name <=> b.name})

        return [characters: characters, project: project]
    }

    def chooseStep(ResearchProject project) {
        def character = Character.findById(params.characterId)

        def visibleTrees = []

        project.trees.each({ it ->
            def visibleTree = determineVisibleTree(it, character)
            visibleTrees.push(visibleTree)
        })

        return [
                project: project,
                character: character,
                trees: visibleTrees
        ]
    }

    def determineVisibleTree(ResearchSkillTree tree, Character character) {
        def visibleTree = [ skillType: tree.type ]

        Skill characterSkill = determineAppropriateSkill(tree, character)

        int skillAvailable = determineHighestVisibleLevelViaSkills(tree, characterSkill)

        tree.steps.each {
            it.metaClass.canSolve = (skillAvailable >= it.level);
        }

        visibleTree["characterSkill"] = characterSkill
        visibleTree["steps"] = tree.steps

        return visibleTree
    }

    Skill determineAppropriateSkill(ResearchSkillTree tree, Character character) {
        Skill appropriateSkill = character.skills.find { it -> it.type == tree.type }
        if (appropriateSkill == null) {
            appropriateSkill = new Skill(type: tree.type, level: 0, usedOnEvent: true)
        }
        return appropriateSkill
    }

    Integer determineHighestVisibleLevelViaSkills(ResearchSkillTree tree, Skill appropriateSkill) {
        if (appropriateSkill == null) {
            return 0
        } else {
            return Math.min (appropriateSkill.level, tree.steps.max { it -> it.level }.level )
        }
    }

    def startStep(ResearchProject project) {
        def character = Character.findById(params.characterId)
        def step = ResearchStep.findById(params.stepId)
        String skillTypeName = params.skillType

        String message = researchService.start(step, character, skillTypeName, project)

        flash.message = message
        redirect([action: "chooseStep", id: project.id, params: [characterId: character.id]])
    }

    def resetData() {
        dataInitService.resetData()
        flash.message = "Data reset: alle data is gewist en opnieuw ingeladen."
        redirect([uri: "/"])
    }

    def completeStep(ResearchProject project) {
        def character = Character.findById(params.characterId)
        def step = ResearchStep.findById(params.stepId)
        String skillTypeName = params.skillType


        boolean complete = researchService.finish(project, step, skillTypeName)

        flash.message = "Onderzoek voltooid. Resultaat: ${step.result}"

        if (complete) {
            flash.message = "Project voltooid! (Resultaat stap: ${step.result})"
        }
        redirect([action: "chooseStep", id: project.id, params: [characterId: character.id]])
    }

    def newEvent() {
        dataInitService.newEvent()
        flash.message = "Nieuw evenement begonnen: alle skills zijn weer inzetbaar en alle niet afgeronde stappen zijn gestopt."
        redirect([uri: "/"])
    }

}
