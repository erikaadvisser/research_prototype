package org.eos.research


import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional

@Transactional(readOnly = true)
class ResearchProjectController {

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        respond ResearchProject.list(params), model: [researchProjectInstanceCount: ResearchProject.count()]
    }

    def show(ResearchProject researchProjectInstance) {
        respond researchProjectInstance
    }

    def create() {
        respond new ResearchProject(params)
    }

    @Transactional
    def save(ResearchProject researchProjectInstance) {
        if (researchProjectInstance == null) {
            notFound()
            return
        }

        if (researchProjectInstance.hasErrors()) {
            respond researchProjectInstance.errors, view: 'create'
            return
        }

        researchProjectInstance.save flush: true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.created.message', args: [message(code: 'researchProject.label', default: 'ResearchProject'), researchProjectInstance.id])
                redirect researchProjectInstance
            }
            '*' { respond researchProjectInstance, [status: CREATED] }
        }
    }

    def edit(ResearchProject researchProjectInstance) {
        respond researchProjectInstance
    }

    @Transactional
    def update(ResearchProject researchProjectInstance) {
        if (researchProjectInstance == null) {
            notFound()
            return
        }

        if (researchProjectInstance.hasErrors()) {
            respond researchProjectInstance.errors, view: 'edit'
            return
        }

        researchProjectInstance.save flush: true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'ResearchProject.label', default: 'ResearchProject'), researchProjectInstance.id])
                redirect researchProjectInstance
            }
            '*' { respond researchProjectInstance, [status: OK] }
        }
    }

    @Transactional
    def delete(ResearchProject researchProjectInstance) {

        if (researchProjectInstance == null) {
            notFound()
            return
        }

        researchProjectInstance.delete flush: true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'ResearchProject.label', default: 'ResearchProject'), researchProjectInstance.id])
                redirect action: "index", method: "GET"
            }
            '*' { render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'researchProject.label', default: 'ResearchProject'), params.id])
                redirect action: "index", method: "GET"
            }
            '*' { render status: NOT_FOUND }
        }
    }
}
