package org.eos.research


import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional

@Transactional(readOnly = true)
class ResearchSkillTreeController {

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        respond ResearchSkillTree.list(params), model: [researchSkillTreeInstanceCount: ResearchSkillTree.count()]
    }

    def show(ResearchSkillTree researchSkillTreeInstance) {
        respond researchSkillTreeInstance
    }

    def create() {
        respond new ResearchSkillTree(params)
    }

    @Transactional
    def save(ResearchSkillTree researchSkillTreeInstance) {
        if (researchSkillTreeInstance == null) {
            notFound()
            return
        }

        if (researchSkillTreeInstance.hasErrors()) {
            respond researchSkillTreeInstance.errors, view: 'create'
            return
        }

        researchSkillTreeInstance.save flush: true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.created.message', args: [message(code: 'researchSkillTree.label', default: 'ResearchSkillTree'), researchSkillTreeInstance.id])
                redirect researchSkillTreeInstance
            }
            '*' { respond researchSkillTreeInstance, [status: CREATED] }
        }
    }

    def edit(ResearchSkillTree researchSkillTreeInstance) {
        respond researchSkillTreeInstance
    }

    @Transactional
    def update(ResearchSkillTree researchSkillTreeInstance) {
        if (researchSkillTreeInstance == null) {
            notFound()
            return
        }

        if (researchSkillTreeInstance.hasErrors()) {
            respond researchSkillTreeInstance.errors, view: 'edit'
            return
        }

        researchSkillTreeInstance.save flush: true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'ResearchSkillTree.label', default: 'ResearchSkillTree'), researchSkillTreeInstance.id])
                redirect researchSkillTreeInstance
            }
            '*' { respond researchSkillTreeInstance, [status: OK] }
        }
    }

    @Transactional
    def delete(ResearchSkillTree researchSkillTreeInstance) {

        if (researchSkillTreeInstance == null) {
            notFound()
            return
        }

        researchSkillTreeInstance.delete flush: true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'ResearchSkillTree.label', default: 'ResearchSkillTree'), researchSkillTreeInstance.id])
                redirect action: "index", method: "GET"
            }
            '*' { render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'researchSkillTree.label', default: 'ResearchSkillTree'), params.id])
                redirect action: "index", method: "GET"
            }
            '*' { render status: NOT_FOUND }
        }
    }
}
