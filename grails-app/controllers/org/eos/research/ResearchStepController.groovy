package org.eos.research


import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional

@Transactional(readOnly = true)
class ResearchStepController {

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        respond ResearchStep.list(params), model: [researchStepInstanceCount: ResearchStep.count()]
    }

    def show(ResearchStep researchStepInstance) {
        respond researchStepInstance
    }

    def create() {
        respond new ResearchStep(params)
    }

    @Transactional
    def save(ResearchStep researchStepInstance) {
        if (researchStepInstance == null) {
            notFound()
            return
        }

        if (researchStepInstance.hasErrors()) {
            respond researchStepInstance.errors, view: 'create'
            return
        }

        researchStepInstance.save flush: true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.created.message', args: [message(code: 'researchStep.label', default: 'ResearchStep'), researchStepInstance.id])
                redirect researchStepInstance
            }
            '*' { respond researchStepInstance, [status: CREATED] }
        }
    }

    def edit(ResearchStep researchStepInstance) {
        respond researchStepInstance
    }

    @Transactional
    def update(ResearchStep researchStepInstance) {
        if (researchStepInstance == null) {
            notFound()
            return
        }

        if (researchStepInstance.hasErrors()) {
            respond researchStepInstance.errors, view: 'edit'
            return
        }

        researchStepInstance.save flush: true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'ResearchStep.label', default: 'ResearchStep'), researchStepInstance.id])
                redirect researchStepInstance
            }
            '*' { respond researchStepInstance, [status: OK] }
        }
    }

    @Transactional
    def delete(ResearchStep researchStepInstance) {

        if (researchStepInstance == null) {
            notFound()
            return
        }

        researchStepInstance.delete flush: true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'ResearchStep.label', default: 'ResearchStep'), researchStepInstance.id])
                redirect action: "index", method: "GET"
            }
            '*' { render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'researchStep.label', default: 'ResearchStep'), params.id])
                redirect action: "index", method: "GET"
            }
            '*' { render status: NOT_FOUND }
        }
    }
}
