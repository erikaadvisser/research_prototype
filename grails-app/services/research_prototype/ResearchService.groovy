package research_prototype

import grails.transaction.Transactional
import org.eos.research.Character
import org.eos.research.ResearchProject
import org.eos.research.ResearchSkillTree
import org.eos.research.ResearchStep
import org.eos.research.Skill
import org.eos.research.SkillType
import org.eos.research.StepStatus

@Transactional
class ResearchService {

    String start(ResearchStep researchStep, Character character, String skillTypeName, ResearchProject project) {
        SkillType type = SkillType.valueOf(skillTypeName)

        Skill skill = character.skills.find { it -> it.type == type }

        skill.attach()
        skill.timesUsedOnEvent += 1
        skill.save()

        if (researchStep.empty) {
            return processEmpty(researchStep, project, type)
        }
        else {
            researchStep.attach()
            researchStep.researcher = character
            researchStep.status = StepStatus.IN_PROGRESS
            researchStep.save()

            return "Start onderzoek: ${researchStep.quest}"
        }
    }

    String processEmpty(ResearchStep step, ResearchProject project, SkillType type) {
        ResearchSkillTree tree = project.trees.find { it.type == type }

        int level = step.level

        tree.steps.findAll { it.level >= level }.each {
            it.status = StepStatus.SOLVED
        }

        return "Op dit niveau hoeft er niets te gebeuren."
    }


    boolean finish(ResearchProject project, ResearchStep step, String skillTypeName) {
        SkillType type = SkillType.valueOf(skillTypeName)

        step.attach()
        step.researcher = null
        step.status = StepStatus.SOLVED
        step.save()

        ResearchSkillTree tree = project.trees.find { it.type == type }
        ResearchStep nextStep = tree.steps.find { it.level == step.level + 1 }
        if (nextStep.empty) {
            processEmpty(nextStep, project, type)
        }

        boolean finished = isProjectFinished(project)
        if (finished) {
            project.attach()
            project.completed = true
            project.save()
        }
        return finished
    }

    boolean isProjectFinished(ResearchProject project) {
        ResearchSkillTree unfinishedTree = project.trees.find { tree ->
            tree.steps.find { step -> step.status != StepStatus.SOLVED } != null
        }

        return (unfinishedTree == null)
    }
}
