package research_prototype

import grails.transaction.Transactional
import org.eos.research.Character
import org.eos.research.ResearchProject
import org.eos.research.ResearchSkillTree
import org.eos.research.ResearchStep
import org.eos.research.Skill
import org.eos.research.SkillType
import org.eos.research.StepStatus

@Transactional
class DataInitService {

    def initData() {
	
// Bionische spin mag als test project aangezet worden. In de uitleg die ik naar de pilotters stuur leg ik uit dat de bionische spin het test project is. 	
       createResearchProjectBionischeSpin()
//        createResearchProjectBloedVanVreemdeWezens()
        createResearchProjectFloraScanner()
        createResearchProjectGeothermischeEnergie()
        createResearchProjectTuriPantser()

        createCharacters()
    }

    def createCharacters() {

// er zijn maar 4 personen die alle skills verkrijgen voor de pilot. In de pilot uitleg geef ik aan dat zij de enige zijn die de knoppen mogen bedienen, maar dat ze wel de mensen moeten vinden die de skills hebben.
	
        new Character(name: "Aurelia Vesta Lapis Helena")
                .addSkill(SkillType.INGENIEUR, 10)
                .addSkill(SkillType.INFORMATICA, 10)
                .addSkill(SkillType.GENEESKUNDE, 10)
                .addSkill(SkillType.TELEPSYCHICA, 10)
                .addSkill(SkillType.SOCIOLOGIE, 10)
                .addSkill(SkillType.POLITICOLOGIE, 10)
                .addSkill(SkillType.ECONOMIE, 10)
                .addSkill(SkillType.GEOLOGIE, 10)
                .addSkill(SkillType.BIOCHEMIE, 10)
                .save();	
 
//        *****************************************

       new Character(name: "Lucius Naevius")
                .addSkill(SkillType.INGENIEUR, 10)
                .addSkill(SkillType.INFORMATICA, 10)
                .addSkill(SkillType.GENEESKUNDE, 10)
                .addSkill(SkillType.TELEPSYCHICA, 10)
                .addSkill(SkillType.SOCIOLOGIE, 10)
                .addSkill(SkillType.POLITICOLOGIE, 10)
                .addSkill(SkillType.ECONOMIE, 10)
                .addSkill(SkillType.GEOLOGIE, 10)
                .addSkill(SkillType.BIOCHEMIE, 10)
                .save();	
 
//        *****************************************

       new Character(name: "Lucia Acilia")
                .addSkill(SkillType.INGENIEUR, 10)
                .addSkill(SkillType.INFORMATICA, 10)
                .addSkill(SkillType.GENEESKUNDE, 10)
                .addSkill(SkillType.TELEPSYCHICA, 10)
                .addSkill(SkillType.SOCIOLOGIE, 10)
                .addSkill(SkillType.POLITICOLOGIE, 10)
                .addSkill(SkillType.ECONOMIE, 10)
                .addSkill(SkillType.GEOLOGIE, 10)
                .addSkill(SkillType.BIOCHEMIE, 10)
                .save();	
 
//        *****************************************

       new Character(name: "Mikhyl Ghourazji")
                .addSkill(SkillType.INGENIEUR, 10)
                .addSkill(SkillType.INFORMATICA, 10)
                .addSkill(SkillType.GENEESKUNDE, 10)
                .addSkill(SkillType.TELEPSYCHICA, 10)
                .addSkill(SkillType.SOCIOLOGIE, 10)
                .addSkill(SkillType.POLITICOLOGIE, 10)
                .addSkill(SkillType.ECONOMIE, 10)
                .addSkill(SkillType.GEOLOGIE, 10)
                .addSkill(SkillType.BIOCHEMIE, 10)
                .save();	
 
//        *****************************************

//        new Character(name: "GENERIC TEMPLATE OP VOLGORDE VAN EXCELSHEET")
//                .addSkill(SkillType.INGENIEUR, 2)
//                .addSkill(SkillType.INFORMATICA, 1)
//                .addSkill(SkillType.GENEESKUNDE, 2)
//                .addSkill(SkillType.TELEPSYCHICA, 1)
//                .addSkill(SkillType.SOCIOLOGIE, 2)
//                .addSkill(SkillType.POLITICOLOGIE, 2)
//                .addSkill(SkillType.ECONOMIE, 2)
//                .addSkill(SkillType.GEOLOGIE, 2)
//                .addSkill(SkillType.BIOCHEMIE, 1)
//                .save();

    }
    /**
     * Research project: bionische spin
     * - biochemie 5
     * b1. Celtype
     * b2. Oorsprong
     * b3. Zwaktes
     * b4. Energiehuishouding
     * b5. Brein
     *
     * - mechanica 2
     * e1. algehele constructie
     * e2. klimmen
     */
    def createResearchProjectBionischeSpin() {
        def b1 = new ResearchStep(level: 1, quest: SkillType.BIOCHEMIE.level_1_quest, result: "100% Natuurlijke cellen").save()
        def b2 = new ResearchStep(level: 2, quest: "Onderzoek oorsprong cellen", result: "Cellen komen uit een mengelmoes van bestaande dieren...").save()
        def b3 = new ResearchStep(level: 3, quest: "Onderzoek op zwaktes", result: "Biochemische samenstelling is erg kwetsbaar voor basische omgeving (PH > 11)").save()
        def b4 = new ResearchStep(level: 4, quest: "Onderzoek energiehuishouding", result: "Er is geen duidelijk voedingssysteem, voeding wordt waarschijnlijk direct via de huid opgenomen.").save()
        def b5 = new ResearchStep(level: 5, quest: "Onderzoek zenuwstelsel", result: "Zenuwstelsel lijkt erg op dat van een Araneidae (Wielwebspin)").save()

        def e1 = new ResearchStep(level: 1, quest: SkillType.INGENIEUR.level_1_quest, result: "Een mechanisch skelet maar alle spieren zijn biologisch").save()
        def e2 = new ResearchStep(level: 2, quest: "Onderzoek de poten", result: "Uitklapbare haakjes onder de poten zorgen ervoor dat de spin tegen bomen op kan lopen lopen.").save()

        def bioTree = new ResearchSkillTree(type: SkillType.BIOCHEMIE, steps: [b1, b2, b3, b4, b5]).save()
        def mechanicaTree = new ResearchSkillTree(type: SkillType.INGENIEUR, steps: [e1, e2]).save()

        def project = new ResearchProject(
                name: "Bionische spin",
                description: "Een bionische spin is gevonden in het bos, deze wordt onderzocht.",
                rewardDescription: "De bionische spin is onderzocht en kan nu gereproduceerd worden.",
                trees: [bioTree, mechanicaTree]).save()

        addMissingSteps(project)

    }

    /**
     * Research project: Bloed van de vreemde Wezens
     * - biochemie 5
     * b1. Celtype
     * b2. Oorsprong
     * b3. Zwaktes
     * b4. Energiehuishouding
     * b5. Zenuwstelsel
     * b6. Brein
     */
    def createResearchProjectBloedVanVreemdeWezens() {
        def b1 = new ResearchStep(level: 1, quest: SkillType.BIOCHEMIE.level_1_quest, result: "Er zitten in de database geen overeenkomsten met een eerder geanalyseerd monster. Er is 1 monster die het meest afwijkt van alles wat je ooit eerder gezien hebt, waar deze tree mee verder gaat").save()
        def b2 = new ResearchStep(level: 2, quest: "Onderzoek oorsprong cellen", result: "De celstructuren zijn volledig symmetrisch").save()
        def b3 = new ResearchStep(level: 3, quest: "Onderzoek op zwaktes", result: "De celstructeren van een van de monsters bestaat volledig uit eiwitten.").save()
        def b4 = new ResearchStep(level: 4, quest: "Onderzoek energiehuishouding", result: "De celstructuren van het monster dat volledig uit eiwitten bestaat, heeft geen DNA codering. De energiehuishouding lijkt volledig te bestaan op het opeten van de eigen celstructuur").save()
        def b5 = new ResearchStep(level: 5, quest: "Onderzoek zenuwstelsel", result: "Het zenuwstelsel van het eiwitten-monster zijn de cellen van het wezen die als laatste overblijven binnen de energiehuishouding.").save()
        def b6 = new ResearchStep(level: 6, quest: "Onderzoek brein", result: "Het voed zich door het eten van zichzelf en is vrij primitief. De schatting is tussen het niveau van een kat en een aap. Dit is het hoogste onderzoeksniveau binnen deze researchtree").save()

        def bioTree = new ResearchSkillTree(type: SkillType.BIOCHEMIE, steps: [b1, b2, b3, b4, b5,b6]).save()

        def project = new ResearchProject(
                name: "Bloed van de vreemde wezens",
                description: "Het bloed van de vreemde wezens is geharvest en kan onderzocht worden",
                rewardDescription: "Er blijken monsters van verschillende vreemde wezens genomen te zijn. Vervolgonderzoek is mogelijk om een biologisch wapen te maken voor het eiwit-monster",
                trees: [bioTree]).save()

        addMissingSteps(project)
    }

    def addMissingSteps(ResearchProject project) {
        def allTypesLeft = new HashSet<SkillType>(SkillType.values().toList())

        project.trees.each { tree ->
            addMissingStepsForTree(tree)
            allTypesLeft.remove(tree.type)
        }

        allTypesLeft.each { type ->
            ResearchSkillTree tree = new ResearchSkillTree(type: type, steps: []).save()

            def extraStep = new ResearchStep(level: 1, quest: tree.type.level_1_quest, result: tree.type.level_1_result).save()
            tree.steps.add(extraStep)


            for (int i = 2; i <= 10; i++) {
                addExtraStep(tree, i)
            }
            tree.save()
            project.trees.add(tree)
        }
        project.save()
    }

    def addMissingStepsForTree(ResearchSkillTree tree) {
        int highestNonEmptyStep = tree.steps.max { it.level }.level

        for (int i = highestNonEmptyStep + 1; i <= 10; i++) {
            addExtraStep(tree, i)
        }

        tree.save()
    }

    def addExtraStep(ResearchSkillTree tree, int level) {
        def extraStep = new ResearchStep(level: level, empty: true).save()
        tree.steps.add(extraStep)
    }

    def resetData() {
        def steps = ResearchStep.findAll()
        ResearchStep.deleteAll(steps)

        def trees = ResearchSkillTree.findAll()
        ResearchSkillTree.deleteAll(trees)

        def projects = ResearchProject.findAll()
        ResearchProject.deleteAll(projects)

        def skills = Skill.findAll()
        Skill.deleteAll(skills)

        def characters = Character.findAll()
        Character.deleteAll(characters)

        initData()
    }

    def newEvent() {
        def steps = ResearchStep.findAll()
        steps.each { step ->
            if (step.status == StepStatus.IN_PROGRESS) {
                step.status = StepStatus.OPEN
                step.researcher = null
                step.save()
            }
        }

        def skills = Skill.findAll()
        skills.each { skill ->
            skill.timesUsedOnEvent = 0
            skill.save()
        }
    }

    /**
     * Research project: Ecoform onderzoeksdata: Flora­scanner
     * - INGENIEUR
     * i1. "Onderzoek de scanner"
     * i2. "Onderzoek de modificatie"
     * i3. "Onderzoek de demper­chip"
     * i4. "Onderzoek de feedback­loop"
     * i5. "Combineer alle data van voorgaande niveau's"
     *
     * - telepsychica
     * t1. "Raak de scanner aan"
     * t2.  "Blijf de scanner ondanks de schokjes vasthouden"
     * t3.  "Bid naar Mair"
     * t4.  "Interpreteer het gezamenlijke visioen"
     *
     * informatica
     * h1. "Acces de scanner"
     * h2. "Check de logs"
     * h3. "Check de laatste wijzigingen"
     * h4. "Graaf door de code heen"
     * h5. "Haal 5 MOEILIJKE sodoku's op bij de SL om diepbegraven gegevens beschikbaar te krijgen"
     *
     * biochemie
     * b1. "Informatica 5 moet eerst zijn afgerond: Bekijk de data uit de scanner, die de informatici ontdekt hebben"
     * b2. "Informatica 5 moet eerst zijn afgerond: Bekijk de data uit de scanner, die de informatici ontdekt hebben"
     * b3. "Informatica 5 moet eerst zijn afgerond: Bekijk de data uit de scanner, die de informatici ontdekt hebben"
     * b4. "Informatica 5 moet eerst zijn afgerond: Bekijk de data uit de scanner, die de informatici ontdekt hebben"
     * b5. "Informatica 5 moet eerst zijn afgerond: Bekijk de data uit de scanner, die de informatici ontdekt hebben"
     *het achterliggende idee is dat ze met deze scanner uiteindelijk telepsychische planten kunnen vinden, waar ze een goed verkopende drug van kunnen maken, waardoor er een export industrie kan ontstaan van in eerste instantie legale (want onbekende) drugs.
     */
    def createResearchProjectFloraScanner() {
        def i1 = new ResearchStep(level: 1, quest: "Onderzoek de scanner", result: "Het is een gemodificeerde scanner die de biochemische samenstelling bekijkt met extra functionaliteit").save()
        def i2 = new ResearchStep(level: 2, quest: "Onderzoek de modificatie", result: "De modificatie is een rudimentaire aanpassing van de chip die gebruikt wordt om telepsychica te dempen").save()
        def i3 = new ResearchStep(level: 3, quest: "Onderzoek de demper­chip", result: "Deze chip is gemodificeerd op zo'n manier dat het alles wat het scant probeert te dempen, waarna een feedback­loop ontstaat").save()
        def i4 = new ResearchStep(level: 4, quest: "Onderzoek de feedback­loop", result: "De demper pulseert, maar zodra de demper succesvol dempt, probeert hij daarna nogmaals te dempen, maar met meer energie.").save()
        def i5 = new ResearchStep(level: 5, quest: "Combineer alle data van voorgaande niveau's", result: "Naast de biochemische samenstelling van planten, checkt dit apparaat of een plant telepsychisch is en checkt daarna hoe sterk de telepsychica is.").save()

        def t1 = new ResearchStep(level: 1, quest: "Raak de scanner aan", result: "Je krijgt een 5­tal elektrische schokjes, snel achter elkaar. (tenzij je minder hebt dan 5 niveau's in telepsychica in welk geval je je niveau aan schokjes krijgt).").save()
        def t2 = new ResearchStep(level: 2, quest: "Blijf de scanner ondanks de schokjes vasthouden", result: "Je merkt dat het apparaat je telepsychiche gaven beinvloed, maar slechts heel zwakjes. Je beste gok is dat het je concentratie moet verstoren, maar in de praktijk zou dat onmogelijk zijn").save()
        def t3 = new ResearchStep(level: 3, quest: "Bid naar Mair", result: "Mair geeft je een goed gevoel over de scanner en geeft je een kortstondig visioen van een zieke walvis die op zoek gaat naar eten en stenen probeert te eten").save()
        def t4 = new ResearchStep(level: 4, quest: "Interpreteer het gezamenlijke visioen", result: "Er zijn veel verschillende invalshoeken mogelijk, maar jullie komen er uiteindelijk (naast andere speculaties) op uit dat Mair voor deze scanner is, maar dat het niet in de handen van de vreemde wezens mag vallen").save()

        def h1 = new ResearchStep(level: 1, quest: "Acces de scanner", result: "Toegang verkrijgen is heel simpel, het is een standaard protocol wat gebruikt wordt om mensen snel toegang te geven").save()
        def h2 = new ResearchStep(level: 2, quest: "Check de logs", result: "De reden dat er gemakkelijk toegang tot te krijgen moest zijn, is dat er 3 informatici aan werkten, die allemaal updates schreven op basis van 5 biochemici").save()
        def h3 = new ResearchStep(level: 3, quest: "Check de laatste wijzigingen", result: "De laatste aanpassingen proberen de sterkte van het apparaat te vergroten, het aantal scanbare soorten uit te breiden en een efficientere feedback­loop te maken").save()
        def h4 = new ResearchStep(level: 4, quest: "Graaf door de code heen", result: "De basis van de software is tweeledig: Een biochemisch programma gericht op het scannen van planten en de software van een telepsychica dempende helm").save()
        def h5 = new ResearchStep(level: 5, quest: "Haal 5 MOEILIJKE sodoku's op bij de SL om diepbegraven gegevens beschikbaar te krijgen", result: "Je unlocked de biochemie vakjes, biochemisten mogen deze nu omdraaien").save()

        def b1 = new ResearchStep(level: 1, quest: "Informatica 5 moet eerst zijn afgerond: Bekijk de data uit de scanner, die de informatici ontdekt hebben", result: "Om een of andere reden hebben ze deze scanner gebruikt om elk plantje in sight te scannen.").save()
        def b2 = new ResearchStep(level: 2, quest: "Informatica 5 moet eerst zijn afgerond: Bekijk de data uit de scanner, die de informatici ontdekt hebben", result: "Niet alleen scannen ze elk plantje op deze planeet, ze scannen zelfs dezelfde planten meerdere keren").save()
        def b3 = new ResearchStep(level: 3, quest: "Informatica 5 moet eerst zijn afgerond: Bekijk de data uit de scanner, die de informatici ontdekt hebben", result: "Ze zijn bijna obsessief bezig geweest om de scanner steeds efficienter te laten scannen naar bepaalde spoorelementen").save()
        def b4 = new ResearchStep(level: 4, quest: "Informatica 5 moet eerst zijn afgerond: Bekijk de data uit de scanner, die de informatici ontdekt hebben", result: "Deze spoor elementen lijken echter helemaal niets te doen, toch zijn ze ervan overtuigd dat dit de sleutel moet zijn").save()
        def b5 = new ResearchStep(level: 5, quest: "Informatica 5 moet eerst zijn afgerond: Bekijk de data uit de scanner, die de informatici ontdekt hebben", result: "Ze hebben daadwerkelijk een viertal verschillende planten gescand die tijdelijke telepsychisch waren. Sindsdien zijn ze als een gek bezig geweest om deze planten te vinden... om op te eten....").save()

        def e1 = new ResearchStep(level: 1, quest: "Bekijk de waarde van de scanner", result: "De scanner is nog maar maximaal 10 sonuren waard").save()


        def ingenieurTree = new ResearchSkillTree(type: SkillType.INGENIEUR, steps: [i1, i2, i3, i4, i5]).save()
        def telepsychicaTree = new ResearchSkillTree(type: SkillType.TELEPSYCHICA, steps: [t1, t2, t3, t4]).save()
        def informaticaTree = new ResearchSkillTree(type: SkillType.INFORMATICA, steps: [h1, h2, h3, h4, h5]).save()
        def biochemieTree = new ResearchSkillTree(type: SkillType.BIOCHEMIE, steps: [b1, b2, b3, b4, b5]).save()
        def economieTree = new ResearchSkillTree(type: SkillType.ECONOMIE, steps: [e1]).save()

        def project = new ResearchProject(
                name: "Ecoform onderzoeksdata: Flora­scanner",
                description: "Ecoform heeft een state of the art ICC- protocollair research systeem geinstalleerd staan, waar nog oude data in staat. Dit is een van de researchprojecten waar ze mee bezig waren",
                rewardDescription: "Je kunt nu de scanner gebruiken om telepsyschiche planten te vinden. Op basis van de data is het naar alle waarschijnlijkheid mogelijk om hier een extreem goede party-drug van te maken.",
                trees: [ingenieurTree, telepsychicaTree, informaticaTree, biochemieTree, economieTree]).save()

        addMissingSteps(project)

    }


/**
 * Research project: "Ecoform onderzoeksdata: Geothermische energie"
 * geologie
 * g1. "Onderzoek de bestaande data"
 * g2. "Calibreer je drilboor zodat hij gevoelig is voor het detecteren van warmtestromen onder het bodemoppervlak"
 * g3. "Stel een onderzoeksstrategie op en detecteer in de omgeving van het Bastion gedurende meerdere momenten op de dag."
 * g4. "Voer een thermische computer simulatie uit voor het bepalen van de haalbaarheid."
 * g5. "Maak een risico analyse Voer een thermische computer simulatie uit voor het bepalen van de haalbaarheid."
 *
 * ingenieur
 * i1. "Maak een herbeoordeling of het huidige energiessysteem geschikt is voor extra energie die mogelijk aan het systeem wordt toegevoegd"
 * i2.  "ontwerp een warmtewisselaar die extreem warme water omzet naar energie"
 * i3.  "Bepaald hoe om te gaan met corresiviteit en radioactiviteit die vrijkomen bij warme water wat wordt opgepompt"
 *
 * politicologie
 * p1. "Doe navraag of er al verzoeken zijn voor ondersteuning van geologische experts om geothermie mogelijk te maken bij het ICC"
 * p2. "Plaats samen met iemand met minimaal economie niveau 5 een verzoek bij het ICC voor geologische expertse om een geothermische installatie mogelijk te maken "
 *
 */

    def createResearchProjectGeothermischeEnergie() {
        def g1 = new ResearchStep(level: 1, quest:  "Onderzoek de bestaande data", result: "Ecoform zijn aan het kijken naar locaties in de directe omgeving van het Bastion die kansrijk zijn voor geothermie. De eerste metingen geven een tegenstrijdig beeld").save()
        def g2 = new ResearchStep(level: 2, quest: "Calibreer je drilboor zodat hij gevoelig is voor het detecteren van warmtestromen onder het bodemoppervlak", result: "Je drilboor is geschikt om geothermische activiteit te registreren").save()
        def g3 = new ResearchStep(level: 3, quest: "Stel een onderzoeksstrategie op en detecteer in de omgeving van het Bastion gedurende meerdere momenten op de dag.", result: "Er is voldoende data verzameld over de geothermische activiteit in de omgeving van het Bastion").save()
        def g4 = new ResearchStep(level: 4, quest: "Voer een thermische computer simulatie uit voor het bepalen van de haalbaarheid.", result: "De kans dat een doublet meer oplevert als 5Mw is 100%, 10 Mw is meer als 75% en 12 Mw is 45%.").save()
        def g5 = new ResearchStep(level: 5, quest:  "Maak een risico analyse Voer een thermische computer simulatie uit voor het bepalen van de haalbaarheid.", result: "De kans dat een doublet meer oplevert als 5Mw is 100%, 10 Mw is meer als 75% en 12 Mw is 45%.").save()

        def i1 = new ResearchStep(level: 1, quest:  "Maak een herbeoordeling of het huidige energiessysteem geschikt is voor extra energie die mogelijk aan het systeem wordt toegevoegd", result: "Het huidige energievoorzieningssysteem is niet geschikt").save()
        def i2 = new ResearchStep(level: 2, quest: "ontwerp een warmtewisselaar die extreem warme water omzet naar energie", result: "Je beschikt over een lijst met onderdelen die nodig zijn voor warmtewisselaar").save()
        def i3 = new ResearchStep(level: 3, quest: "Bepaald hoe om te gaan met corresiviteit en radioactiviteit die vrijkomen bij warme water wat wordt opgepompt", result: "Dit vraagt om hogere kwaliteit materiaal en totaal gesloten systeem tussen ondergronds en bovengronds.").save()

        def p1 = new ResearchStep(level: 1, quest: "Doe navraag of er al verzoeken zijn voor ondersteuning van geologische experts om geothermie mogelijk te maken bij het ICC", result: "De Sona hebben via Ecoform een bijzonder verzoek gehad om een uniek systeem te realiseren onder complexe mstandigheden").save()
        def p2 = new ResearchStep(level: 2, quest: "Plaats samen met iemand met minimaal economie niveau 5 een verzoek bij het ICC voor geologische expertse om een geothermische installatie mogelijk te maken ", result: "Het verzoek wordt in behandeling genomen als een haalbaarheidstudie is uitgevoerd, risico analyse en systeemaanpassingen inzichtelijk zijn gemaakt. ").save()

        def geologieTree = new ResearchSkillTree(type: SkillType.GEOLOGIE, steps: [g1, g2, g3, g4, g5]).save()
        def ingenieurTree = new ResearchSkillTree(type: SkillType.INGENIEUR, steps: [i1, i2, i3]).save()
        def politicologieTree = new ResearchSkillTree(type: SkillType.POLITICOLOGIE, steps: [p1, p2]).save()

        def project = new ResearchProject(
                name: "Ecoform onderzoeksdata: Geothermische energie",
                description: "Ecoform heeft een state of the art ICC- protocollair research systeem geinstalleerd staan, waar nog oude data in staat. Dit is een van de researchprojecten waar ze mee bezig waren",
                rewardDescription: "Het is vastgesteld dat er grote potentie is voor geothermie. Dit biedt een alternatieve energiebron die continu energie levert. Een formeel verzoek bij het ICC levert een duidelijk vervolg op. Experts kunnen worden ingeroepen om voor advies. Na akkoord kan gestart worden met de volgende fase: veldsimulaties en aanpassingen energievoorziening.",
                trees: [geologieTree, ingenieurTree, politicologieTree]).save()

        addMissingSteps(project)

    }

    def createResearchProjectTuriPantser() {
        def i1 = new ResearchStep(level: 1, quest: "Ga door de data heen", result: "Snel gezien hebben ze meerdere overeenkomende conclusies, maar er valt 1 ding op: Ze hebben een test gedaan met een tonische triller in de kogel").save()
        def i2 = new ResearchStep(level: 2, quest: "Onderzoek tonische triller", result: "Deze tonische triller creeert op het moment van impact een een gerichte trilling buiten het gehoorsspectrum").save()
        def i3 = new ResearchStep(level: 3, quest: "Onderzoek de richting van de triller", result: "De tonische triller richt zijn trillingen in een hoek van precies 60% naast het punt van inslag, je vind ook een drietal tonische kogels").save()
        def i4 = new ResearchStep(level: 4, quest: "Laat iemand met minimaal ballistiek 4 met een tonische kogel op het Turi pantser schieten ", result: "De kogel lijkt op het oog geen verbetering, maar de meetgegevens geven aan dat er potentie is. Je vind ook een fout in de calibratie van de kogel, het moet niet 60% zijn, maar 64,73%.").save()
        def i5 = new ResearchStep(level: 5, quest: "Combineer alle data van voorgaande niveau's", result: "Deze versie van de tonische kogel gaat als een mes door boter heen. Het specifieke magnetische veld van Eos maakt het calibreren van de kogel uitermate makkelijk. Je unlocked politicologie en economie").save()
def i6 = new ResearchStep(level: 6, quest: "Analyseer de compositie", result: "Het lukt jullie om te achterhalen wat de voorgaande researchers niet lukte: Het Turi pantser is gemaakt van foltium, dalium met nog een nieuw element wat jullie nog niet kennen. Het nieuwe element is echter onbenaderbaar zolang het foltium aanwezig is.").save()
def i7 = new ResearchStep(level: 7, quest: "Extraheer het foltium", result: "Foltium lijkt een organische metaal (verbinder), het is op zich niet vormvast").save()
def i8 = new ResearchStep(level: 8, quest: "Analyseer het residu na extractie van het foltium", result: "foltium werkte als lijmstof tussen biochemische componenten en geologische componenten waardoor deze versmelten").save()
def i9 = new ResearchStep(level: 9, quest: "Test het onbekende element.", result: "Het onbekende element heeft een elektrische weerstand van 85 en een hardheid van 392 MPa").save()
def i10 = new ResearchStep(level: 10, quest: "Test het onbekende element", result: "Het onbekende element lost slecht op, heeft een hoge tot zeer hoge reactiviteit, en een warmtegeleiding van 0.1.").save()




		
        def e1 = new ResearchStep(level: 1, quest: "Ingenieur 5 eerst nodig: Schat de waarde van deze kogel in", result: "Op dit moment heeft jullie baas, het ICC, een theoretische monopolie positie. Gezien het belang van deze ontdekking en de doelstelling van het ICC, is de kans klein dat er een cutthroat monopolie nagestreefd gaat worden.").save()
        def e2 = new ResearchStep(level: 2, quest: "Ingenieur 5 eerst nodig: Onderzoek de marktpositie", result: "Deze tonische kogels kunnen in theorie overal gefabriceerd worden. Eos heeft het voordeel dat hier het juiste magnetische veld hangt, wat elders gesimuleerd moet worden. Eos heeft ook op de lange termijn een comparitatief voordeel t.o.v. de rest van het universum").save()
        def e3 = new ResearchStep(level: 3, quest: "Ingenieur 5 eerst nodig: Onderzoek de potentiele winstfactor", result: "Het ICC gaat hier een sowieso een winstmarge op pakken. Als ze het aandurven kan dit een zeer hoge marge zijn: Hoe hoger de marge hoe minder kogelproductie. De afweging is minder produceren en potentieel de oorlog verliezen, of een dikke winstmarge.").save()
        def e4 = new ResearchStep(level: 4, quest: "Ingenieur 5 eerst nodig: Onderzoek de first mover advantage", result: "Door te investeren om productie op te zetten op het Bastion kunnen we first move advantage pakken. Hoewel de inkomensrevenue naar het ICC gaat, kunnen we er wel voor zorgen dat onze mensen ten alle tijden voorzien worden van tonische kogels.").save()

        def p1 = new ResearchStep(level: 1, quest:  "Vraag bij het ICC wat ze van plan zijn met deze resultaten", result: "De algemene consensus is dat deze informatie te belangrijk is om alleenrecht op te houden. De blueprints zullen aan alle (die geen betalingsachterstand hebben) facties verstrekt worden.").save()
        def p2 = new ResearchStep(level: 2, quest: "Welke marge gaat het ICC hanteren op de kogels", result: "Het is nu al duidelijk dat de kogels in high demand zullen zijn, dus zal er een vierdubbele marge gehanteerd worden (opslag van 40%.). De funds komen in de algemene middelen ").save()
        def p3 = new ResearchStep(level: 3, quest: "Probeer een percentage afdracht te regelen", result: "Een vaste afdracht is onbespreekbaar. Dit geeft wel ruimte om te onderhandelen om budgetten te verruimen. Het conclaaf mag besluiten om het budget van 3 aandachtsgebieden met 50% te verhogen").save()
        def p4 = new ResearchStep(level: 4, quest: "Dwing af dat het ICC een investering om gelijk te beginnen met productie 100% terugbetaald", result: "Dit is verbazingwekkend makkelijk en wordt gelijk toegezegd.").save()

        def ingenieurTree = new ResearchSkillTree(type: SkillType.INGENIEUR, steps: [i1, i2, i3, i4, i5, i6, i7, i8, i9, i10]).save()
        def economieTree = new ResearchSkillTree(type: SkillType.ECONOMIE, steps: [e1, e2, e3, e4]).save()
        def politicologieTree = new ResearchSkillTree(type: SkillType.POLITICOLOGIE, steps: [p1, p2, p3, p4]).save()

        def project = new ResearchProject(
                name: "Ecoform onderzoeksdata: Turi pantser",
                description: "Ecoform heeft een state of the art ICC- protocollair research systeem geinstalleerd staan, waar nog oude data in staat. Dit is een van de researchprojecten waar ze mee bezig waren",
                rewardDescription: "Het is mogelijk om deze kogels en masse te produceren op Eos, waardoor er permanent tonische kogels beschikbaar zijn voor mensen op Eos. Verder mag het conclaaf voor 3 aandachtsgebieden 50% meer budget vragen. Als laatste is verder onderzoek mogelijk, in welk geval er onderzoeksaanvragen ingestuurd moeten worden.",
                trees: [ingenieurTree, economieTree, politicologieTree]).save()

        addMissingSteps(project)
    }
}
