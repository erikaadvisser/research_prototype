package org.eos.research

class ResearchProject {


    long id
    String name
    String description
    String rewardDescription
    boolean completed = false
    Set<ResearchSkillTree> trees

    static hasMany = [trees: ResearchSkillTree]

    static constraints = {
    }

    static mapping = {
        description column: "description", sqlType: "varchar", length: 1024
        rewardDescription column: "rewardDescription", sqlType: "varchar", length: 1024
    }

}
