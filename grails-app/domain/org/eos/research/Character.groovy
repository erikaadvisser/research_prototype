package org.eos.research

class Character {

    long id
    String name
    Set<Skill> skills = new HashSet<>()

    static hasMany = [skills: Skill]

    static constraints = {
    }

    Character addSkill(SkillType type, int level) {
        Skill skill = new Skill(type: type, level: level).save()
        skills.add(skill)
        return this
    }
}
