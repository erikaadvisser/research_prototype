package org.eos.research

class Skill {

    long id
    SkillType type
    int level
    int timesUsedOnEvent = 0

    static constraints = {
    }


    public String toString() {
        return "${type?.name} ${level}";
    }
}
