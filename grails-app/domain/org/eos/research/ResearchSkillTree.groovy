package org.eos.research

class ResearchSkillTree {

    long id
    SkillType type
    Set<ResearchStep> steps;

    static hasMany = [steps: ResearchStep]

    static constraints = {
    }
}
