package org.eos.research

class ResearchStep {

    long id
    int level
    boolean empty = false
    StepStatus status = StepStatus.OPEN
    String quest
    String result
    Character researcher

    static constraints = {
        researcher nullable: true
        quest nullable: true
        result nullable: true
    }

    static mapping = {
        quest column: "quest", sqlType: "varchar", length: 1024
        result column: "result", sqlType: "varchar", length: 1024
    }
}
