import org.eos.research.*
import research_prototype.DataInitService

class BootStrap {

    DataInitService dataInitService

    def init = { servletContext ->
        dataInitService.initData()
    }
    def destroy = {
    }

}
