<%@ page import="org.eos.research.ResearchSkillTree" %>



<div class="fieldcontain ${hasErrors(bean: researchSkillTreeInstance, field: 'steps', 'error')} ">
	<label for="steps">
		<g:message code="researchSkillTree.steps.label" default="Steps" />
		
	</label>
	

</div>

<div class="fieldcontain ${hasErrors(bean: researchSkillTreeInstance, field: 'type', 'error')} required">
	<label for="type">
		<g:message code="researchSkillTree.type.label" default="Type" />
		<span class="required-indicator">*</span>
	</label>
	<g:select name="type" from="${org.eos.research.SkillType?.values()}" keys="${org.eos.research.SkillType.values()*.name()}" required="" value="${researchSkillTreeInstance?.type?.name()}" />

</div>

