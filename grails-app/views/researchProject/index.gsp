
<%@ page import="org.eos.research.ResearchProject" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'researchProject.label', default: 'ResearchProject')}" />
		<title><g:message code="default.list.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#list-researchProject" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
				<li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>
			</ul>
		</div>
		<div id="list-researchProject" class="content scaffold-list" role="main">
			<h1><g:message code="default.list.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
				<div class="message" role="status">${flash.message}</div>
			</g:if>
			<table>
			<thead>
					<tr>
					
						<g:sortableColumn property="completed" title="${message(code: 'researchProject.completed.label', default: 'Completed')}" />
					
						<g:sortableColumn property="description" title="${message(code: 'researchProject.description.label', default: 'Description')}" />
					
						<g:sortableColumn property="name" title="${message(code: 'researchProject.name.label', default: 'Name')}" />
					
						<g:sortableColumn property="rewardDescription" title="${message(code: 'researchProject.rewardDescription.label', default: 'Reward Description')}" />
					
					</tr>
				</thead>
				<tbody>
				<g:each in="${researchProjectInstanceList}" status="i" var="researchProjectInstance">
					<tr class="${(i % 2) == 0 ? 'even' : 'odd'}">
					
						<td><g:link action="show" id="${researchProjectInstance.id}">${fieldValue(bean: researchProjectInstance, field: "completed")}</g:link></td>
					
						<td>${fieldValue(bean: researchProjectInstance, field: "description")}</td>
					
						<td>${fieldValue(bean: researchProjectInstance, field: "name")}</td>
					
						<td>${fieldValue(bean: researchProjectInstance, field: "rewardDescription")}</td>
					
					</tr>
				</g:each>
				</tbody>
			</table>
			<div class="pagination">
				<g:paginate total="${researchProjectInstanceCount ?: 0}" />
			</div>
		</div>
	</body>
</html>
