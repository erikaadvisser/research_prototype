<%@ page import="org.eos.research.ResearchProject" %>



<div class="fieldcontain ${hasErrors(bean: researchProjectInstance, field: 'completed', 'error')} ">
	<label for="completed">
		<g:message code="researchProject.completed.label" default="Completed" />
		
	</label>
	<g:checkBox name="completed" value="${researchProjectInstance?.completed}" />

</div>

<div class="fieldcontain ${hasErrors(bean: researchProjectInstance, field: 'description', 'error')} required">
	<label for="description">
		<g:message code="researchProject.description.label" default="Description" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="description" required="" value="${researchProjectInstance?.description}"/>

</div>

<div class="fieldcontain ${hasErrors(bean: researchProjectInstance, field: 'name', 'error')} required">
	<label for="name">
		<g:message code="researchProject.name.label" default="Name" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="name" required="" value="${researchProjectInstance?.name}"/>

</div>

<div class="fieldcontain ${hasErrors(bean: researchProjectInstance, field: 'rewardDescription', 'error')} required">
	<label for="rewardDescription">
		<g:message code="researchProject.rewardDescription.label" default="Reward Description" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="rewardDescription" required="" value="${researchProjectInstance?.rewardDescription}"/>

</div>

