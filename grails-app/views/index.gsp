<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main"/>
		<title>Welcome to Grails</title>

	</head>
	<body>
		<div id="page-body" role="main">
			<h1>Frontier research manager prototype</h1>
			<g:if test="${flash?.message}">
				<p><div class="label label-warning">${flash.message}</div></p>
			</g:if>
			<p>Het prototype om te kijken hoe het idee zou werken voor een voor research projecten.</p>
			<hr>
			<p><g:link controller="research" action="chooseProject">Start</g:link> door het project te kiezen dat wordt onderzocht.</p>
			<hr>
			Soms is het nuttig om de test-data te resetten.
			<br>
			<br> Start een nieuw event: alle characters kunnen hun vaardigheden weer inzetten. Alle niet afgeronde stappen moeten opnieuw gestart worden. [<g:link controller="research" action="newEvent">Nieuw evenement</g:link>]
			<br> Reset alle data (alles) [<g:link controller="research" action="resetData">Reset!</g:link>]
			<hr>
			<br>
			<p><span class="text-danger badge">Onder de motorkap</span> Hieronder vind je toegang tot de onderliggende data. Dit zijn erg ongebruikersvriendelijk, automatisch gegenereerde pagina's.</p>

			<div id="controller-list" role="navigation">
				<h4 class="text-danger">Ruwe Data:</h4>
				<ul>
					<li><g:link controller="character">character</g:link></li>
					<li><g:link controller="skill">skill</g:link></li>
				</ul>
			</div>
		</div>
	</body>
</html>
