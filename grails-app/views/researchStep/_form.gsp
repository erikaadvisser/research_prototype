<%@ page import="org.eos.research.ResearchStep" %>



<div class="fieldcontain ${hasErrors(bean: researchStepInstance, field: 'level', 'error')} required">
	<label for="level">
		<g:message code="researchStep.level.label" default="Level" />
		<span class="required-indicator">*</span>
	</label>
	<g:field name="level" type="number" value="${researchStepInstance.level}" required=""/>

</div>

<div class="fieldcontain ${hasErrors(bean: researchStepInstance, field: 'quest', 'error')} required">
	<label for="quest">
		<g:message code="researchStep.quest.label" default="Quest" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="quest" required="" value="${researchStepInstance?.quest}"/>

</div>

<div class="fieldcontain ${hasErrors(bean: researchStepInstance, field: 'result', 'error')} required">
	<label for="result">
		<g:message code="researchStep.result.label" default="Result" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="result" required="" value="${researchStepInstance?.result}"/>

</div>

<div class="fieldcontain ${hasErrors(bean: researchStepInstance, field: 'revealed', 'error')} ">
	<label for="revealed">
		<g:message code="researchStep.revealed.label" default="Revealed" />
		
	</label>
	<g:checkBox name="revealed" value="${researchStepInstance?.revealed}" />

</div>

<div class="fieldcontain ${hasErrors(bean: researchStepInstance, field: 'solved', 'error')} ">
	<label for="solved">
		<g:message code="researchStep.solved.label" default="Solved" />
		
	</label>
	<g:checkBox name="solved" value="${researchStepInstance?.solved}" />

</div>

