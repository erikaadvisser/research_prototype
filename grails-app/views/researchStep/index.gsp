
<%@ page import="org.eos.research.ResearchStep" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'researchStep.label', default: 'ResearchStep')}" />
		<title><g:message code="default.list.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#list-researchStep" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
				<li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>
			</ul>
		</div>
		<div id="list-researchStep" class="content scaffold-list" role="main">
			<h1><g:message code="default.list.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
				<div class="message" role="status">${flash.message}</div>
			</g:if>
			<table>
			<thead>
					<tr>
					
						<g:sortableColumn property="level" title="${message(code: 'researchStep.level.label', default: 'Level')}" />
					
						<g:sortableColumn property="quest" title="${message(code: 'researchStep.quest.label', default: 'Quest')}" />
					
						<g:sortableColumn property="result" title="${message(code: 'researchStep.result.label', default: 'Result')}" />
					
						<g:sortableColumn property="revealed" title="${message(code: 'researchStep.revealed.label', default: 'Revealed')}" />
					
						<g:sortableColumn property="solved" title="${message(code: 'researchStep.solved.label', default: 'Solved')}" />
					
					</tr>
				</thead>
				<tbody>
				<g:each in="${researchStepInstanceList}" status="i" var="researchStepInstance">
					<tr class="${(i % 2) == 0 ? 'even' : 'odd'}">
					
						<td><g:link action="show" id="${researchStepInstance.id}">${fieldValue(bean: researchStepInstance, field: "level")}</g:link></td>
					
						<td>${fieldValue(bean: researchStepInstance, field: "quest")}</td>
					
						<td>${fieldValue(bean: researchStepInstance, field: "result")}</td>
					
						<td><g:formatBoolean boolean="${researchStepInstance.revealed}" /></td>
					
						<td><g:formatBoolean boolean="${researchStepInstance.solved}" /></td>
					
					</tr>
				</g:each>
				</tbody>
			</table>
			<div class="pagination">
				<g:paginate total="${researchStepInstanceCount ?: 0}" />
			</div>
		</div>
	</body>
</html>
