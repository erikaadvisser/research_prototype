
<%@ page import="org.eos.research.ResearchStep" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'researchStep.label', default: 'ResearchStep')}" />
		<title><g:message code="default.show.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#show-researchStep" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
				<li><g:link class="list" action="index"><g:message code="default.list.label" args="[entityName]" /></g:link></li>
				<li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>
			</ul>
		</div>
		<div id="show-researchStep" class="content scaffold-show" role="main">
			<h1><g:message code="default.show.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			<ol class="property-list researchStep">
			
				<g:if test="${researchStepInstance?.level}">
				<li class="fieldcontain">
					<span id="level-label" class="property-label"><g:message code="researchStep.level.label" default="Level" /></span>
					
						<span class="property-value" aria-labelledby="level-label"><g:fieldValue bean="${researchStepInstance}" field="level"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${researchStepInstance?.quest}">
				<li class="fieldcontain">
					<span id="quest-label" class="property-label"><g:message code="researchStep.quest.label" default="Quest" /></span>
					
						<span class="property-value" aria-labelledby="quest-label"><g:fieldValue bean="${researchStepInstance}" field="quest"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${researchStepInstance?.result}">
				<li class="fieldcontain">
					<span id="result-label" class="property-label"><g:message code="researchStep.result.label" default="Result" /></span>
					
						<span class="property-value" aria-labelledby="result-label"><g:fieldValue bean="${researchStepInstance}" field="result"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${researchStepInstance?.revealed}">
				<li class="fieldcontain">
					<span id="revealed-label" class="property-label"><g:message code="researchStep.revealed.label" default="Revealed" /></span>
					
						<span class="property-value" aria-labelledby="revealed-label"><g:formatBoolean boolean="${researchStepInstance?.revealed}" /></span>
					
				</li>
				</g:if>
			
				<g:if test="${researchStepInstance?.solved}">
				<li class="fieldcontain">
					<span id="solved-label" class="property-label"><g:message code="researchStep.solved.label" default="Solved" /></span>
					
						<span class="property-value" aria-labelledby="solved-label"><g:formatBoolean boolean="${researchStepInstance?.solved}" /></span>
					
				</li>
				</g:if>
			
			</ol>
			<g:form url="[resource:researchStepInstance, action:'delete']" method="DELETE">
				<fieldset class="buttons">
					<g:link class="edit" action="edit" resource="${researchStepInstance}"><g:message code="default.button.edit.label" default="Edit" /></g:link>
					<g:actionSubmit class="delete" action="delete" value="${message(code: 'default.button.delete.label', default: 'Delete')}" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" />
				</fieldset>
			</g:form>
		</div>
	</body>
</html>
