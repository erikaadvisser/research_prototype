<html>
<head>
    <meta name="layout" content="main">
</head>
<body>

<body>
<div class="row">
    <div class="col-lg-3 text-right">
        Project
    </div>
    <div class="col-lg-3">
        <strong>${project.name}</strong>
    </div>
</div>
<div class="row">
    <div class="col-lg-3 text-right">

    </div>
    <div class="col-lg-9">
        ${project.description}
    </div>
</div>
<br>
<div class="row">
    <strong>Kies het karakter dat onderzoek gaat doen</strong>
    <hr>
    <ul>
        <g:each in="${characters}" var="character">
            <li><g:link action="chooseStep" id="${project.id}" params="${[characterId: character.id]}">${character.name}</g:link></li>
        </g:each>
    </ul>
</div>
</body>
</html>