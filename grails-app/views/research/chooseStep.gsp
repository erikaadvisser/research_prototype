<%@ page import="org.eos.research.SkillUse" contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <meta name="layout" content="main">
    <title>Research</title>
</head>
<div class="row">
    <div class="col-lg-3 text-right">
        Project
    </div>
    <div class="col-lg-3">
        <strong>${project.name}</strong>
    </div>
</div>
<div class="row">
    <div class="col-lg-3 text-right">

    </div>
    <div class="col-lg-9">
        ${project.description}
    </div>
</div>
<br>
<div class="row">
    <div class="col-lg-3 text-right">
        Onderzoeker
    </div>
    <div class="col-lg-9">
        <strong>${character.name}</strong>
        <br>
        <ul>
            <g:each in="${character.skills.sort( { it.level})}" var="skill">
                <li>
                    <g:if test="${skill.timesUsedOnEvent >= org.eos.research.SkillUse.MAX_USES_ON_EVENT }">
                        <i class="fa fa-fw fa-check-square-o"></i>
                        <span style="color: gray">
                            ${skill.type.name} ${skill.level} - Al gebruikt dit evenement</span>
                    </g:if>
                    <g:else>
                        <i class="fa fa-fw fa-square-o"></i>
                        ${skill.type.name} ${skill.level}
                    </g:else>
                </li>
            </g:each>
        </ul>

    </div>
</div>

<div class="row">
    <div class="col-lg-3 text-right">
        Status
    </div>
    <div class="col-lg-9">
        <g:if test="${project.completed}">
            <span class="label label-success">Onderzoek afgerond</span><br>
            <strong>${project.rewardDescription}</strong>
        </g:if>
        <g:else>
            <span class="label label-info">Onderzoek loopt</span><br>
        </g:else>
    </div>
</div>
    <br>

    <div class="row">
        %{--<strong>Overzicht onderzoeksstappen</strong>--}%
        <g:if test="${flash?.message}">
            <p><br><span class="label label-warning">Melding:</span> ${flash.message}</p>
        </g:if>
    <ul>
        <g:each in="${trees.sort{ it.skillType.name} }" var="tree">
            <hr>
            <strong>Skill: ${tree.skillType.name}</strong>
            <p>
                Jouw skill level: ${tree.characterSkill.level }
                (kan nog <span class="strong">${org.eos.research.SkillUse.MAX_USES_ON_EVENT - tree.characterSkill.timesUsedOnEvent} </span> keer gebruikt worden dit evenement)
                %{--<br>Hoogste bekende stap: ${tree.maxLevel}--}%
            </p>
            <ul>
                <g:each in="${tree.steps.sort({ it.level })}" var="step">
                    <li>
                        Level ${step.level} -
                        <g:if test="${step.status == org.eos.research.StepStatus.SOLVED}">
                            <g:if test="${step.empty}">
                                <span class="label label-warning">None</span>
                            </g:if>
                            <g:else>
                                <span class="label label-success">Done</span>
                                ${step.quest} <i class="fa fa-lightbulb-o"></i> <em>${step.result}</em>
                            </g:else>
                        </g:if>
                        <g:if test="${step.status == org.eos.research.StepStatus.IN_PROGRESS}">
                            <span class="label label-danger">Work</span>
                            <g:if test="${step.researcher.id == character.id}">
                                <em><strong>${step.quest}</strong></em> <i class="fa fa-star-half-o"></i>
                                <g:link action="completeStep" id="${project.id}" params="${[characterId: character.id, stepId: step.id, skillType: tree.skillType]}">Onderzoek gedaan</g:link>
                            </g:if>
                            <g:else>
                                ${step.quest}</> <i class="fa fa-user"></i>
                                Onderzoek wordt gedaan door <strong>${step.researcher.name}</strong>
                            </g:else>
                        </g:if>
                        <g:if test="${step.status == org.eos.research.StepStatus.OPEN}">
                            <span class="label label-info">Todo</span>
                            <g:if test="${step.canSolve}">
                                <g:if test="${tree.characterSkill?.timesUsedOnEvent < org.eos.research.SkillUse.MAX_USES_ON_EVENT}">
                                    <g:link action="startStep" id="${project.id}" params="${[characterId: character.id, stepId: step.id, skillType: tree.skillType]}">Start</g:link>
                                </g:if>
                                <g:else>
                                    <span class="text-muted">Skill al gebruikt</span>
                                </g:else>
                            </g:if>
                            <g:else>
                                <span class="text-muted">Skill te laag</span>
                            </g:else>
                        </g:if>
                    </li>
                </g:each>
            </ul>
        </g:each>
    </ul>
</p>
</div>
</body>
</html>