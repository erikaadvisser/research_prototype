<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <meta name="layout" content="main">
    <title>Kies character</title>
</head>
<body>
<div class="row">
    <strong>Kies het project om aan te werken.</strong>
    <hr>
    <ul>
        <g:each in="${projects}" var="project">
            <li><g:link action="chooseResearcher" id="${project.id}">${project.name}</g:link></li>
        </g:each>
    </ul>
</div>
</body>
</html>