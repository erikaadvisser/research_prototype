<%@ page import="org.eos.research.Character" %>



<div class="fieldcontain ${hasErrors(bean: characterInstance, field: 'name', 'error')} required">
	<label for="name">
		<g:message code="character.name.label" default="Name" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="name" required="" value="${characterInstance?.name}"/>

</div>

<div class="fieldcontain ${hasErrors(bean: characterInstance, field: 'skills', 'error')} ">
	<label for="skills">
		<g:message code="character.skills.label" default="Skills" />
		
	</label>
	<g:select name="skills" from="${org.eos.research.Skill.list()}" multiple="multiple" optionKey="id" size="5" value="${characterInstance?.skills*.id}" class="many-to-many"/>

</div>

