<!DOCTYPE html>
<!--[if lt IE 7 ]> <html lang="en" class="no-js ie6"> <![endif]-->
<!--[if IE 7 ]>    <html lang="en" class="no-js ie7"> <![endif]-->
<!--[if IE 8 ]>    <html lang="en" class="no-js ie8"> <![endif]-->
<!--[if IE 9 ]>    <html lang="en" class="no-js ie9"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--> <html lang="en" class="no-js"><!--<![endif]-->
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<title><g:layoutTitle default="Grails"/></title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<link rel="shortcut icon" href="${assetPath(src: 'favicon.ico')}" type="image/x-icon">
		<asset:stylesheet src="bootstrap.min.css" />
		<asset:stylesheet src="font-awesome-4.2.0/css/font-awesome.min.css"/>
		<asset:javascript src="application.js"/>
		<title>Eos Research Prototype</title>
		<g:layoutHead/>
	</head>
	<body>
		<div class="container">
			<div class="row text-center">
					<br>
					<g:link uri="/" ><img width="200"  src="http://www.rubenlinde.nl/eos/images/headers/Frontier_header.jpg"/></g:link>
					<br>
					<g:link  uri="/">Begin</g:link>
					- - <g:link controller="research" action="chooseProject">Project</g:link>
					<g:if test="${project}">
						- - <g:link controller="research" action="chooseResearcher" id="${project.id}">Onderzoeker</g:link>
					</g:if>
			</div>
			<div class="row text-center">
				<h2>Eos research</h2>
			</div>
			<g:layoutBody/>
		</div>
	</body>
</html>
