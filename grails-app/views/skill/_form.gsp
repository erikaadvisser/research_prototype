<%@ page import="org.eos.research.Skill" %>



<div class="fieldcontain ${hasErrors(bean: skillInstance, field: 'level', 'error')} required">
	<label for="level">
		<g:message code="skill.level.label" default="Level" />
		<span class="required-indicator">*</span>
	</label>
	<g:field name="level" type="number" value="${skillInstance.level}" required=""/>

</div>

<div class="fieldcontain ${hasErrors(bean: skillInstance, field: 'type', 'error')} required">
	<label for="type">
		<g:message code="skill.type.label" default="Type" />
		<span class="required-indicator">*</span>
	</label>
	<g:select name="type" from="${org.eos.research.SkillType?.values()}" keys="${org.eos.research.SkillType.values()*.name()}" required="" value="${skillInstance?.type?.name()}" />

</div>

<div class="fieldcontain ${hasErrors(bean: skillInstance, field: 'usedOnEvent', 'error')} ">
	<label for="usedOnEvent">
		<g:message code="skill.usedOnEvent.label" default="Used On Event" />
		
	</label>
	<g:checkBox name="usedOnEvent" value="${skillInstance?.usedOnEvent}" />

</div>

