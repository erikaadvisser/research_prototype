package org.eos.research

/**
 *
 */
enum StepStatus {
    OPEN,
    IN_PROGRESS,
    SOLVED

}