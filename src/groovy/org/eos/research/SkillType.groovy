package org.eos.research

/**
 * Enum for skills
 */
enum SkillType {
    INFORMATICA("Informatica", "Voer een wifi scan uit", "Er is geen activiteit waargenomen"),
    GENEESKUNDE("Geneeskunde", "Voer een scan uit op giftige gassen", "Het specimen is niet toxisch"),
    SOCIOLOGIE("Sociologie", "Controleer of er al iets publiekelijk bekend is over dit onderwerp", "Er is niets publiekelijk bekend over dit onderwerp"),
    ECONOMIE("Economie", "Doe onderzoek naar de waarde op basis van een vage omschrijving", "Op basis hiervan kan geen waarde worden vastgesteld"),
    GEOLOGIE("Geologie", "Lees literatuur na of dit geologische relevantie heeft", "Niets gevonden"),
    BIOCHEMIE("Biochemie", "Leg het onder de microscoop", "Niets relevants ontdekt"),
    TELEPSYCHICA("Telepsychica", "Raak het voorwerp aan", "Je merkt niets speciaals"),
    INGENIEUR("Ingenieur", "Onderzoek de algehele constructie", "Geen interessante details aan de constructie gevonden"),
    POLITICOLOGIE("Politicologie", "?", "?")


    public final String name
    public final String level_1_quest
    public final String level_1_result


    private SkillType(String name, String level_1_quest, String level_1_result) {
        this.name = name
        this.level_1_quest = level_1_quest
        this.level_1_result = level_1_result
    }

}